var username = '';
var game = new Phaser.Game(800, 600, Phaser.CANVAS);
var myInput;
function getInfo() {
    FB.api('/me', 'GET', {fields: 'first_name,last_name,name,id'}, function (response) {
        localStorage.setItem('username', response.name);
        if(myInput != undefined) {
            myInput.destroy();
        }
        if(StartState.nameField != undefined) {
            StartState.nameField.destroy();
        }
        StartState.myInput = StartState.createInput(100, 260, response.name);
        StartState.nameField = game.add.sprite(100, 260, 'nameField');
    });
}
var choosedBg = 'background4';
var timer = 0;
var total = 0;
var score = 0;
var lives;
var userName;
var scoreString = '';
var scoreText;
var soundKill;
var swatterBat;

game.state.add('StartState', StartState);
game.state.add('GameState', GameState);
game.state.add('GameOverState', GameOverState);
game.state.add('ChoseBgState', ChoseBgState);
game.state.start('StartState');
