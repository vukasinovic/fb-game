var ChoseBgState = {
    preload: function() {
        game.canvas.id = 'game';
        this.load.atlas('mysprite', 'assets/img/spritesheet.png', 'assets/img/sprites.json');
        this.load.image('background' , 'assets/img/MenuBg.jpg');
        this.load.image('basement', 'assets/img/basement-icon.png');
        this.load.image('livingRoom', 'assets/img/living-room-icon.png');
        this.load.image('original', 'assets/img/original-icon.png');
        this.load.image('woodCabin', 'assets/img/woods-cabin-icon.png');
        this.load.image('back', 'assets/img/back.png');
        this.load.image('startFight', 'assets/img/start-fight.png');

        this.load.image('basementTxt', 'assets/img/basement.png');
        this.load.image('livingRoomTxt', 'assets/img/living-room.png');
        this.load.image('originalTxt', 'assets/img/original.png');
        this.load.image('woodCabinTxt', 'assets/img/woods-cabin.png');
        this.load.image('hand', 'assets/img/hand.png');
    },
    create: function() {
        this.background = this.game.add.sprite(0, 0, 'background');
        this.original = this.game.add.sprite(240, 130, 'original');
        this.original.anchor.setTo(0.5, 0.5);
        this.original.inputEnabled = true;
        this.original.events.onInputDown.add(selectBg1, this);
        this.woodCabin = this.game.add.sprite(555, 130, 'woodCabin');
        this.woodCabin.anchor.setTo(0.5, 0.5);
        this.woodCabin.inputEnabled = true;
        this.woodCabin.events.onInputDown.add(selectBg2, this);
        this.livingRoom = this.game.add.sprite(240, 360, 'livingRoom');
        this.livingRoom.anchor.setTo(0.5, 0.5);
        this.livingRoom.inputEnabled = true;
        this.livingRoom.events.onInputDown.add(selectBg3, this);
        this.basement = this.game.add.sprite(555, 360, 'basement');
        this.basement.anchor.setTo(0.5, 0.5);
        this.basement.inputEnabled = true;
        this.basement.events.onInputDown.add(selectBg4, this);

        this.back = this.game.add.sprite(150, 510, 'back');
        this.back.inputEnabled = true;
        this.back.events.onInputDown.add(mainMenu, this);
        this.originalTxt = this.game.add.sprite(185, 225, 'originalTxt');
        this.woodCabinTxt = this.game.add.sprite(460, 225, 'woodCabinTxt');
        this.livingRoomTxt = this.game.add.sprite(160, 460, 'livingRoomTxt');
        this.basementTxt = this.game.add.sprite(480, 460, 'basementTxt');
        this.hand = this.game.add.sprite(0, 0, 'hand');
    },
    update: function() {
        this.hand.bringToTop();
        this.hand.position.setTo(game.input.mousePointer.x, game.input.mousePointer.y);
    }
};
function choseBg() {
    game.state.start('ChoseBgState');
}
function  selectBg1() {
    choosedBg = 'background4';
    this.add.tween(this.original.scale).to({ x: 1.3, y: 1.3}, 500, Phaser.Easing.Back.Out, true, 0);
    this.add.tween(this.woodCabin.scale).to({ x: 1, y: 1}, 500, Phaser.Easing.Back.Out, true, 0);
    this.add.tween(this.livingRoom.scale).to({ x: 1, y: 1}, 500, Phaser.Easing.Back.Out, true, 0);
    this.add.tween(this.basement.scale).to({ x: 1, y: 1}, 500, Phaser.Easing.Back.Out, true, 0);
}
function  selectBg2() {
    choosedBg = 'background1';
    this.add.tween(this.original.scale).to({ x: 1, y: 1}, 500, Phaser.Easing.Back.Out, true, 0);
    this.add.tween(this.woodCabin.scale).to({ x: 1.3, y: 1.3}, 500, Phaser.Easing.Back.Out, true, 0);
    this.add.tween(this.livingRoom.scale).to({ x: 1, y: 1}, 500, Phaser.Easing.Back.Out, true, 0);
    this.add.tween(this.basement.scale).to({ x: 1, y: 1}, 500, Phaser.Easing.Back.Out, true, 0);
}
function  selectBg3() {
    choosedBg = 'background2';
    this.add.tween(this.original.scale).to({ x: 1, y: 1}, 500, Phaser.Easing.Back.Out, true, 0);
    this.add.tween(this.woodCabin.scale).to({ x: 1, y: 1}, 500, Phaser.Easing.Back.Out, true, 0);
    this.add.tween(this.livingRoom.scale).to({ x: 1.3, y: 1.3}, 500, Phaser.Easing.Back.Out, true, 0);
    this.add.tween(this.basement.scale).to({ x: 1, y: 1}, 500, Phaser.Easing.Back.Out, true, 0);
}
function  selectBg4() {
    choosedBg = 'background3';
    this.add.tween(this.original.scale).to({ x: 1, y: 1}, 500, Phaser.Easing.Back.Out, true, 0);
    this.add.tween(this.woodCabin.scale).to({ x: 1, y: 1}, 500, Phaser.Easing.Back.Out, true, 0);
    this.add.tween(this.livingRoom.scale).to({ x: 1, y: 1}, 500, Phaser.Easing.Back.Out, true, 0);
    this.add.tween(this.basement.scale).to({ x: 1.3, y: 1.3}, 500, Phaser.Easing.Back.Out, true, 0);
}