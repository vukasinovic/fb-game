var clickRect;
var StartState = {
    preload: function() {
        game.canvas.id = 'game';
        this.load.atlas('mysprite', 'assets/img/spritesheet.png', 'assets/img/sprites.json');
        this.load.image('background' , 'assets/img/MenuBg.jpg');
        this.load.image('startFight', 'assets/img/start-fight.png');
        this.load.image('superBoy' , 'assets/img/mozzi-superboy.png');
        this.load.image('choose', 'assets/img/choose.png');
        this.load.image('nameField', 'assets/img/name-field.png');
        this.load.image('hand', 'assets/img/hand.png');
        this.load.image('connectFB', 'assets/img/connectwithFB.png');
    },
    create: function() {
        this.background = this.game.add.sprite(0, 0, 'background');
        this.superBoy = this.game.add.sprite(300, 50, 'superBoy');
        this.background.scale.setTo(1);
        myInput = this.createInput(100, 260, 'enter your name');
        this.nameField = game.add.sprite(100, 260, 'nameField');
        this.startFight = this.game.add.sprite(250, 400, 'startFight');
        this.startFight.inputEnabled = true;
        this.startFight.events.onInputDown.add(startGame, this);
        this.connectFB = this.game.add.sprite(160, 330, 'connectFB');
        this.connectFB.inputEnabled = true;
        this.connectFB.events.onInputDown.add(login, this);
        this.choose = this.game.add.sprite(270, 475, 'choose');
        this.choose.inputEnabled = true;
        this.choose.events.onInputDown.add(choseBg, this);
        this.hand = this.game.add.sprite(0, 0, 'hand');
        clickRect = new Phaser.Rectangle(545,515, 200, 70);
        game.input.onDown.add(handlePointerDown);
    },
    update: function() {
        myInput.canvasInput.render();
        localStorage.setItem('username', myInput.canvasInput._value);
        this.hand.bringToTop();
        this.hand.position.setTo(game.input.mousePointer.x, game.input.mousePointer.y);
    },
    inputFocus: function(sprite){
        sprite.canvasInput.focus();
    },
    createInput: function(x, y, name){
        if(localStorage.getItem('username') != '' || localStorage.getItem('username') != null) {
            name = localStorage.getItem('username');
        }
        var bmd = this.add.bitmapData(800, 100);
        myInput = this.game.add.sprite(x, y, bmd);
        myInput.canvasInput = new CanvasInput({
            canvas: bmd.canvas,
            fontSize: 30,
            fontFamily: 'ArialRoundedMTBold',
            fontWeight: 'bold',
            width: 580,
            height: 50,
            padding: 8,
            borderWidth: 1,
            borderRadius: 20,
            placeHolder: name,
            placeHolderColor: '#ea2227',
            backgroundColor: '#cac8ca',
            fontColor: '#ea2227',
            textAlign: 'center',
            value: name
        });
        myInput.inputEnabled = true;
        myInput.input.useHandCursor = true;
        myInput.events.onInputUp.add(this.inputFocus, this);

        return myInput;
    }
};
function updateUsername() {
    console.log('yo');
}
function startGame() {
    score = 0;
    game.state.start('GameState');
}
function handlePointerDown(pointer){
    var inside = clickRect.contains(pointer.x,pointer.y);
    if(inside == true) {
        window.open("http://www.mozzipatch.com/", "_blank");
    }
}