var killedSprites = [];
var flySpeed = 6000;
var eyesAn;
var scoreBound = 10;
var scoreSet = 10;
var confuse = false;
var GameState = {
    preload: function() {
        this.load.atlas('mysprite', 'assets/img/spritesheet.png', 'assets/img/sprites.json');
        this.load.atlas('eyesSprite', 'assets/img/eyesSprite.png', 'assets/img/eyesSprite.json');
        this.load.image('background1' , 'assets/img/background1.jpg');
        this.load.image('background2' , 'assets/img/background2.jpg');
        this.load.image('background3' , 'assets/img/background3.jpg');
        this.load.image('background4' , 'assets/img/background4.jpg');
        this.load.image('health', 'assets/img/health.png');
        this.load.image('splash', 'assets/img/splash.png');
        this.load.audio('flyKill', ['assets/sound/killSound.mp3']);
    },
    create: function() {
        //Sounds
        soundKill = game.add.audio('flyKill');
        game.physics.startSystem(Phaser.Physics.ARCADE);
        this.background = this.game.add.sprite(0, 0, choosedBg);
        this.background.scale.setTo(1);
        enemyGroupLeft = this.game.add.group(); // create group
        enemyGroupRight = this.game.add.group(); // create group
        swatterBat = game.add.sprite(game.world.centerX, game.world.centerY, 'mysprite');
        swatterBat.animations.add('tepalica', Phaser.Animation.generateFrameNames('tepalica0', 1, 1), 5, true);
        swatterBat.animations.play('tepalica');
        swatterBat.scale.setTo(1);
        swatterBat.anchor.setTo(0.7, 0.6);
        swatterBat.bringToTop();
        scoreString = 'Score : ';
        scoreText = game.add.text(30, 20, scoreString + score, { font: '34px ArialRoundedMTBold', fill: '#f8e717' });
        userName = game.add.text(230, 20, localStorage.getItem('username'), { font: '34px ArialRoundedMTBold', fill: '#ea2227' });
        lives = game.add.group();
        var xCord = 25;
        for (var i = 0; i < 5; i++)
        {
            var bat = lives.create(xCord, 540, 'health');
            xCord += 40;
        }
    },
    update: function() {
        myInput.canvasInput.render();
        myInput.destroy();

        swatterBat.position.setTo(game.input.mousePointer.x, game.input.mousePointer.y);
        if (total < 50000 && this.game.time.now > timer) {
            if(score >= 100) {
                scoreSet = 20;
            }
            if(score <= scoreBound) {
                if(score >= 0 && score < 10 || score >= 20 && score <= 30 || score >= 40 && score <= 50 || score >= 60 && score <= 70 || score >= 80 && score <= 90) {
                    createNewFlyLeft(flySpeed);
                } else {
                    if(score == 100) {
                        flySpeed = 6000;
                    }
                    if(score > 100) {
                        createNewFlyLeft(flySpeed);
                    }
                    createNewFlyRight(flySpeed);
                }
            } else {
                if(flySpeed <= 200) {
                } else {
                    flySpeed = flySpeed - 200;
                }
                scoreBound += scoreSet;
            }

            if(score == 15 && confuse == false) {
                eyesShow();
            }
        }
        enemyGroupLeft.forEach(function(item) {
          if(item.x > game.width) {
              live = lives.getFirstAlive();
              if (live)
              {
                  if(item.alive == true)
                  live.kill();
              }
              if (lives.countLiving() < 1)
              {
                  scoreRef.push({name: localStorage.getItem('username'), score: score, fbLink: localStorage.getItem('fbLink')});
                  game.state.start('GameOverState');
              }
              item.kill();
              item.destroy();
          }
        });
        enemyGroupRight.forEach(function(item) {
            if(item.x < 0) {
                live = lives.getFirstAlive();
                if (live)
                {
                    if(item.alive == true)
                        live.kill();
                }
                if (lives.countLiving() < 1)
                {
                    flySpeed = 6000;
                    scoreBound = 10;
                    scoreSet = 10;
                    game.state.start('GameOverState');
                }
                item.kill();
                item.destroy();
            }
        });
    }
};
function swatterBatClick() {
    swatterBat.animations.add('tepalica-kill', ['tepalica01', 'tepalica02', 'tepalica03','tepalica05','tepalica04','tepalica03','tepalica02', 'tepalica01'], 100, false);
    swatterBat.animations.play('tepalica-kill');
}
function killFly(fly) {
    var splash = game.add.sprite(fly.x, fly.y, 'splash');
    swatterBat.bringToTop();
    splash.anchor.setTo(0.3, 1);
    splash.alpha = 1;
    killedSprites.push(splash);
    killedSprites.forEach(function(sprite) {
        var tween = game.add.tween(sprite);
        tween.to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None);
        tween.onComplete.add(function () {
            sprite.destroy();
        });
        tween.start();
    });
    swatterBatClick();
    soundKill.play();
    score += 1;
    scoreText.text = scoreString + score;
    fly.kill();
}
function createNewFlyLeft(speed) {
    var fly = game.add.sprite(-(Math.random() * 800), game.world.randomY+300, 'mysprite');
    swatterBat.bringToTop();
    fly.animations.add('mosquito-left', Phaser.Animation.generateFrameNames('mosquito-right0', 1, 2), 35, true);
    fly.animations.play('mosquito-left');
    fly.inputEnabled = true;
    fly.input.useHandCursor = true;
    fly.anchor.setTo(0.5);
    fly.events.onInputDown.add(killFly, this);
    fly.checkWorldBounds = true;
    enemyGroupLeft.add(fly);
    var move = game.add.tween(fly);
    var jump = game.add.tween(fly);
    move.to({x: window.outerWidth+ 200}, speed);
    var amplituda = Math.floor((Math.random() * 500) + 1);
    jump.to({y: amplituda}, 2000, vanHalen, true, 0, Number.MAX_VALUE, 0);
    move.start();
    total++;
    timer = game.time.now + 1000;
}

function createNewFlyRight(speed) {
    var fly = game.add.sprite(900, game.world.randomY+300, 'mysprite');
    swatterBat.bringToTop();
    fly.animations.add('mosquito-right', Phaser.Animation.generateFrameNames('mosquito-left0', 1, 2), 35, true);
    fly.animations.play('mosquito-right');
    fly.inputEnabled = true;
    fly.input.useHandCursor = true;
    fly.anchor.setTo(0.5);
    fly.events.onInputDown.add(killFly, this);
    fly.checkWorldBounds = true;
    enemyGroupRight.add(fly);
    var move = game.add.tween(fly);
    var jump = game.add.tween(fly);
    move.to({x: -window.outerWidth-200}, speed);
    var amplituda = Math.floor((Math.random() * 500) + 1);
    jump.to({y: amplituda}, 2000, vanHalen, true, 0, Number.MAX_VALUE, 0);
    move.start();
    total++;
    timer = game.time.now + 1000;
}
function vanHalen (v) {
    return Math.sin(v * Math.PI) * 1;
}

function eyesShow() {
    eyesAn = game.add.sprite(380, 700, 'eyesSprite');
    var tween = game.add.tween(eyesAn);
    var tweenDown = game.add.tween(eyesAn);
    tween.to({ y: 470 }, 500, Phaser.Easing.Linear.None);
    tween.onComplete.add(function () {
        eyesAn.animations.add('eyess', ['eyes01', 'eyes02', 'eyes02', 'eyes01', 'eyes01', 'eyes02', 'eyes02', 'eyes01'], 3, false);
        eyesAn.animations.play('eyess');
        setTimeout(function () {
            tweenDown.start();
        }, 2500);
    });
    tweenDown.to({ y: 700 }, 800, Phaser.Easing.Linear.None);
    tweenDown.onComplete.add(function () {
        setTimeout(function () {
            eyesAn.destroy();
        }, 1500);
    });
    tween.start();
    confuse = true;
}