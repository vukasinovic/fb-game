var GameOverState = {
    preload: function() {
        game.canvas.id = 'game';
        this.load.atlas('mysprite', 'assets/img/spritesheet.png', 'assets/img/sprites.json');
        this.load.image('background' , 'assets/img/MenuBg.jpg');
        this.load.image('hand', 'assets/img/hand.png');
        this.load.image('yourScore', 'assets/img/your-score.png');
        this.load.image('highestScore', 'assets/img/highest-score.png');
        this.load.image('easierWay', 'assets/img/easierway.png');
        this.load.image('mainMenu', 'assets/img/main-menu.png');
        this.load.image('fightAgain', 'assets/img/fight-again.png');
    },
    create: function() {
        this.background = this.game.add.sprite(0, 0, 'background');
        this.yourScore = this.game.add.sprite(310, 210, 'yourScore');
        this.highestScore = this.game.add.sprite(260, 60, 'highestScore');
        this.mainMenu = this.game.add.sprite(60, 480, 'mainMenu');
        this.fightAgain = this.game.add.sprite(540, 480, 'fightAgain');
        this.easierWay = this.game.add.sprite(230, 400, 'easierWay');
        this.easierWay.inputEnabled = true;
        this.easierWay.events.onInputDown.add(goToWebsite, this);
        var highestScoreTxt = game.add.text(490, 60, highScore, { font: '34px ArialRoundedMTBold', fill: '#ea2227' });
        var scorePosition = 355;
        if(score < 10) {
            scorePosition = 390;
        } else if(score < 100) {
            scorePosition = 370;
        } else if (score < 1000) {
            scorePosition = 355;
        }
        var playerScoreTxt = game.add.text(scorePosition, 260, score, { font: '45px ArialRoundedMTBold', fill: '#ea2227' });
        this.mainMenu.inputEnabled = true;
        this.mainMenu.events.onInputDown.add(mainMenu, this);
        this.fightAgain.inputEnabled = true;
        this.fightAgain.events.onInputDown.add(playGame, this);
        this.hand = this.game.add.sprite(0, 0, 'hand');
    },
    update: function() {
        this.hand.bringToTop();
        this.hand.position.setTo(game.input.mousePointer.x, game.input.mousePointer.y);
    }
};
function mainMenu() {
    game.state.start('StartState');
}
function playGame() {
	score = 0;
    game.state.start('GameState');
}
function goToWebsite() {
    window.open("http://www.mozzipatch.com/", "_blank");
}