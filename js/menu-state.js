var clickableRect;
var StartState = {
    preload: function() {
        game.canvas.id = 'game';
        this.load.atlas('mysprite', 'assets/img/spritesheet.png', 'assets/img/sprites.json');
        this.load.image('background' , 'assets/img/MenuBg.jpg');
        this.load.image('startFight', 'assets/img/start-fight.png');
        this.load.image('superBoy' , 'assets/img/mozzi-superboy.png');
        this.load.image('choose', 'assets/img/choose.png');
        this.load.image('nameField', 'assets/img/name-field.png');
        this.load.image('hand', 'assets/img/hand.png');
        this.load.image('howToPlay', 'assets/img/howtoplay.png');
        this.load.image('connectFB', 'assets/img/connectwithFB.png');
    },
    create: function() {
        this.background = this.game.add.sprite(0, 0, 'background');
        this.superBoy = this.game.add.sprite(300, 50, 'superBoy');
        this.background.scale.setTo(1);
        myInput = this.createInput(100, 260, 'enter your name');
        /* NameField */
        this.nameField = game.add.sprite(100, 260, 'nameField');
        /* StartFight Sprite */
        this.startFight = this.game.add.sprite(250, 400, 'startFight');
        this.startFight.inputEnabled = true;
        this.startFight.events.onInputDown.add(startGame, this);
        /* Connect with Facebook Sprite */
        this.connectFB = this.game.add.sprite(160, 330, 'connectFB');
        this.connectFB.inputEnabled = true;
        this.connectFB.events.onInputDown.add(login, this);
        /* Choose Battlefield Sprite */
        this.choose = this.game.add.sprite(400, 475, 'choose');
        this.choose.inputEnabled = true;
        this.choose.events.onInputDown.add(choseBg, this);
        /* How to play Sprite */
        this.howToPlay = this.game.add.sprite(150, 475, 'howToPlay');
        this.howToPlay.inputEnabled = true;
        this.howToPlay.events.onInputDown.add(howToPlayGame, this);
        /* Hand Cursor Sprite */
        this.hand = this.game.add.sprite(0, 0, 'hand');
        //define you regionvar
        clickableRect = new Phaser.Rectangle(545,520,200,70);
        game.input.onDown.add(handlePointerDown);
    },
    update: function() {
        myInput.canvasInput.render();
        if(facebookName) {
            localStorage.setItem('username', facebookName);
        } else {
            localStorage.setItem('username', myInput.canvasInput._value);
        }
        this.hand.bringToTop();
        this.hand.position.setTo(game.input.mousePointer.x, game.input.mousePointer.y);
    },
    inputFocus: function(sprite){
        sprite.canvasInput.focus();
    },
    createInput: function(x, y, name){
        if(localStorage.getItem('username') != '' || localStorage.getItem('username') != null) {
            name = localStorage.getItem('username');
        }
        var bmd = this.add.bitmapData(800, 100);
        myInput = this.game.add.sprite(x, y, bmd);
        myInput.canvasInput = new CanvasInput({
            canvas: bmd.canvas,
            fontSize: 30,
            fontFamily: 'ArialRoundedMTBold',
            fontWeight: 'bold',
            width: 580,
            height: 50,
            padding: 8,
            borderWidth: 1,
            borderRadius: 20,
            placeHolder: 'enter your email',
            placeHolderColor: '#ea2227',
            backgroundColor: '#cac8ca',
            fontColor: '#ea2227',
            textAlign: 'center',
            value: name
        });
        myInput.inputEnabled = true;
        myInput.input.useHandCursor = true;
        myInput.events.onInputUp.add(this.inputFocus, this);

        return myInput;
    }
};
function startGame() {
    score = 0;
    game.state.start('GameState');
}
function handlePointerDown(pointer){
    var inside = clickableRect.contains(pointer.x,pointer.y);
   if(inside) {
       window.open("http://www.mozzipatch.com/", "_blank");
   }
}

function howToPlayGame() {
    window.open("http://www.mozzipatch.com/", "_blank");
}